<!DOCTYPE html>
<html lang="em">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Looping</title>
    </head>
    <body>
        <h2>Contoh soal</h2>
        <?php
            
            $string = "PHP is never old";
            echo "String : ".$string ."<br>";
            echo "Panjang String : " .strlen($string) ."<br>";
            echo "Jumlah Kata : " . str_word_count($string) ."<br>". "<br";
            
            $first_sentence = "Hello PHP!" ; // Panjang string 10, jumlah kata: 2
            echo "kata: ".$first_sentence."<br>";
            echo "Panjang String : " .strlen($first_sentence) ."<br>";
            echo "Jumlah Kata : " . str_word_count($first_sentence) ."<br>". "<br";


            $second_sentence = "I'm ready for the challenges"; // Panjang string: 28,  jumlah kata: 5
            echo "String : ".$second_sentence ."<br>";
            echo "Panjang String : " .strlen($second_sentence) ."<br>";
            echo "Jumlah Kata : " . str_word_count($second_sentence) ."<br>". "<br><br>";

            $string2 = "I love PHP";
        
            echo "<label>String: </label> \"$string2\" <br>";
            echo "<br>Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
            // Lanjutkan di bawah ini
            echo "<br>Kata kedua: " . substr($string2, 2, 4) . "<br>"  ;
            echo "<br> Kata Ketiga: " . substr($string2, 7, 3) . "<br>" ;
    
            echo "<h3> Soal No 3 </h3>";
            /*
                SOAL NO 3
                Mengubah karakter atau kata yang ada di dalam sebuah string.
            */
            $string3 = "PHP is old but sexy!";
            echo "String: \"$string3\" "; 
            echo " <br> Ganti Kalimat : ". str_replace("sexy","awesome",$string3);
            // OUTPUT : "PHP is old but awesome"
        ?>
    </body>
</html>